# Time Capsule
Implementation of self decoding time capsule and its factory. The factory is able to create capsules that need a given time on a given hardware to be decrypted; aka Timed-release Crypto.
Encryption of text files and command line strings are supported (binary is on the way)! The factory performs measurements on the hardware it is run to calculate the number of steps that should be taken before the decryption is finished.
The project is Crypto++ dependent.

# Reference
Rivest, R. L., A. Shamir, and D. A. Wagner. 1996. “Time-Lock Puzzles and Timed-Release Crypto.” Cambridge, MA, USA: Massachusetts Institute of Technology.